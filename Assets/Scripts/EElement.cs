﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EElement 
{
    WATER,
    EARTH,
    AIR,
    FIRE,
    THUNDER,
    COUNT
}

public static class EElementExtentios
{
    private static Dictionary<EElement, List<EElement>> s_AdvantageDictionary = null;

    static EElementExtentios()
    {
        s_AdvantageDictionary = new Dictionary<EElement, List<EElement>>();

        s_AdvantageDictionary.Add(EElement.WATER, new List<EElement>() { EElement.FIRE});
        s_AdvantageDictionary.Add(EElement.EARTH, new List<EElement>() { EElement.THUNDER});
        s_AdvantageDictionary.Add(EElement.AIR, new List<EElement>() { EElement.EARTH});
        s_AdvantageDictionary.Add(EElement.FIRE, new List<EElement>() { EElement.AIR});
        s_AdvantageDictionary.Add(EElement.THUNDER, new List<EElement>() { EElement.WATER});
    }

    public static bool HasAdvantage(this EElement thisElement, EElement other)
    {
        return s_AdvantageDictionary[thisElement].Contains(other);
    }

    public static RuntimeAnimatorController GetAnimationController(this EElement thisElement)
    {
        RuntimeAnimatorController controller = null;

        switch(thisElement)
        {
            case EElement.AIR:
                controller = Resources.Load<RuntimeAnimatorController>("AirEnemyAnimationController");
                break;

            case EElement.EARTH:
                controller = Resources.Load<RuntimeAnimatorController>("GroundEnemyAnimationController");
                break;

            case EElement.FIRE:
                controller = Resources.Load<RuntimeAnimatorController>("FireEnemyAnimationController");
                break;

            case EElement.THUNDER:
                controller = Resources.Load<RuntimeAnimatorController>("ElectricEnemyAnimationController");
                break;

            case EElement.WATER:
                controller = Resources.Load<RuntimeAnimatorController>("WaterEnemyAnimationController");
                break;
        }

        return controller;
    }
}
