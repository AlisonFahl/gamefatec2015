﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Linq;

[CustomEditor(typeof(OrbSpellRelationDictionary))]
public class OrbSpellRelationEditor : Editor {
    override public void OnInspectorGUI()
    {
        var myTarget = (OrbSpellRelationDictionary)target;

        int i = 1;
        foreach (var relation in myTarget.m_Relations)
        {
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Combination " + i);
            relation.m_Spell = (Spell)EditorGUILayout.ObjectField("Spell ", relation.m_Spell, typeof(Spell), true);
            GUI.enabled = false;
            relation.m_TypeOne = (EOrbType)EditorGUILayout.EnumPopup("First Orb", relation.m_TypeOne);
            relation.m_TypeTwo = (EOrbType)EditorGUILayout.EnumPopup("Second Orb", relation.m_TypeTwo);
            relation.m_TypeThree = (EOrbType)EditorGUILayout.EnumPopup("Third Orb", relation.m_TypeThree);
            GUI.enabled = true;

            i++;
        }
    }
}
#endif