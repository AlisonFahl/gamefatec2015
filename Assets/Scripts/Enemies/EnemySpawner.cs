﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour 
{
    [SerializeField]
    private GameObjectPool m_EnemyPool;

    [SerializeField]
    public float m_MinLeft;
    [SerializeField]
    public float m_MaxRight;

    private bool m_CanSpawn = true;
    [SerializeField]
    private int m_SpawnQuantity = 1;
    private float m_IncreaseDificultyTimer = 0;
    private float m_SpawnTimer = 0;
    [SerializeField]
    private float m_SpawnInterval = 10;
    [SerializeField]
    private float m_IncreaseDificultyInterval = 80;

    public void Spawn(int quantity, EElement type)
    {
        for(int i=0; i<quantity; i++)
        {
            var enemy = m_EnemyPool.NextObject();
            enemy.transform.position = new Vector3(Random.Range(m_MinLeft, m_MaxRight), transform.position.y, enemy.transform.position.z);
            enemy.GetComponent<Enemy>().Reset();
            enemy.GetComponent<Enemy>().m_Element = type;
            enemy.GetComponent<Enemy>().Speed = (Random.value * 0.4f) + 0.35f;
            enemy.GetComponent<Animator>().runtimeAnimatorController = type.GetAnimationController();
            enemy.SetActive(true);
        }
    }

    public void Spawn(int quantity)
    {
        for (int i = 0; i < quantity; i++)
        {
            Spawn(1, (EElement)Random.Range(0, (int)EElement.COUNT));
        }
    }

    public void Start()
    {

    }

    public void Update()
    {
#if UNITY_EDITOR
        if(Input.GetKeyDown("k"))
        {
            Spawn(Random.Range(1, 5));
        }
#endif
        if(m_CanSpawn)
        {
            m_IncreaseDificultyTimer += Time.deltaTime;
            while(m_IncreaseDificultyTimer >= m_IncreaseDificultyInterval)
            {
                m_SpawnQuantity++;

                m_IncreaseDificultyTimer -= m_IncreaseDificultyInterval;
            }

            m_SpawnTimer += Time.deltaTime;
            while (m_SpawnTimer >= m_SpawnInterval)
            {
                Spawn(m_SpawnQuantity);

                m_SpawnTimer -= m_SpawnInterval;
            }
        }
    }
}
