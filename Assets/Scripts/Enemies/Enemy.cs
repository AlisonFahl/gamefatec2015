﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
    [SerializeField]
    [Range(0, 1)]
    private float m_Speed;

    [SerializeField]
    private float m_Damage = 5;

    public EElement m_Element;

    private Animator m_Animator;
    private static readonly float MAX_SPEED = 1f;

    private GameObject m_EndPoint;
    private Player m_Player;

    private bool m_IsDead = false;
    private float m_VanishStartTime;
    private static readonly float VANISH_TIME = 1f;

    public float Speed
    {
        get { return m_Speed; }

        set
        {
            m_Speed = value;
            if(m_Speed > MAX_SPEED)
            {
                m_Speed = MAX_SPEED;
            }
        }
    }

    void Start()
    {
        m_Animator = GetComponent<Animator>();

        m_EndPoint = GameObject.Find("EndPoint");
        m_Player = GameObject.Find("Player").GetComponent<Player>();
    }

    void Update()
    {
        m_Animator.speed = (m_Speed / MAX_SPEED);

        if (!m_IsDead)
        {
            Vector3 step = m_Speed * new Vector3(0, -1, 0) * Time.deltaTime;
            transform.Translate(step);
        }
        else
        {
            FadeOut();
        }

        //Check if enemy reached the goal and apply the damage to the player
        if(transform.position.y < m_EndPoint.transform.position.y)
        {
            m_Player.TakeDamage(m_Damage);

            gameObject.SetActive(false);
        }
    }

    public void Reset()
    {
        m_IsDead = false;
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
    }

    public void Kill()
    {
        m_IsDead = true;
        
        OnVanish();
    }

    private void OnVanish()
    {
        m_VanishStartTime = Time.time;
        m_Animator.Stop();
    }

    private void FadeOut()
    {
        var time = Time.time - m_VanishStartTime;

        if (time > VANISH_TIME)
        {
            gameObject.SetActive(false);
        }
        else
        {
            var alpha = Mathf.Lerp(1, 0, time / VANISH_TIME);
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, alpha);
        }
    }

}
