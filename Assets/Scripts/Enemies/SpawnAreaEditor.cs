﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(EnemySpawner))]
public class SpawnAreaEditor : Editor 
{
    void OnSceneGUI()
    {
        EnemySpawner spawner = (EnemySpawner)target;

        Handles.DrawLine(new Vector3(spawner.m_MinLeft, spawner.transform.position.y, spawner.transform.position.z), new Vector3(spawner.m_MaxRight, spawner.transform.position.y, spawner.transform.position.z));
    }
}
#endif