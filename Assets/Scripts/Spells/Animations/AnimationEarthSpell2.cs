﻿using UnityEngine;
using System.Collections;

public class AnimationEarthSpell2 : BaseSpellAnimation 
{
    private Animator[] m_Animators;

    void Awake()
    {
        m_Animators = GetComponentsInChildren<Animator>();
    }

    protected override void OnAnimationStart()
    {
        base.OnAnimationStart();

        foreach (var animation in m_Animators)
        {
            animation.Rebind();
            animation.Play("New Animation");
        }
    }
}
