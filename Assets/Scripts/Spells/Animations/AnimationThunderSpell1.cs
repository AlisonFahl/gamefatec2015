﻿using UnityEngine;
using System.Collections;

public class AnimationThunderSpell1 : BaseSpellAnimation
{
    private Animator m_Animator;

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

    protected override void OnAnimationStart()
    {
        base.OnAnimationStart();

        m_Animator.Rebind();
        m_Animator.Play("New Animation");
    }
}
