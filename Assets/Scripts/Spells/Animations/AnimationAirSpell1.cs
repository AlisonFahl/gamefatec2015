﻿using UnityEngine;
using System.Collections;

public class AnimationAirSpell1 : BaseSpellAnimation
{
    private Animator[] m_Animators;

    private bool m_Translate = false;
    private float m_TranslationInitialTime;

    void Awake()
    {
        m_Animators = GetComponentsInChildren<Animator>();
    }

    void Update()
    {
        if(m_Translate)
        {
            var time = Time.time - m_TranslationInitialTime;

            var x = Mathf.Lerp(1.56f, -2.08f, time / m_AnimationTime);

            foreach (Transform arrow in transform)
            {
                arrow.localPosition = new Vector3(x, arrow.localPosition.y, arrow.localPosition.z);
            }
        }
    }

    protected override void OnAnimationStart()
    {
        base.OnAnimationStart();
        
        m_TranslationInitialTime = Time.time;
        PutArrowsOnInitialPosition();
        m_Translate = true;
        
        foreach (var animation in m_Animators)
        {
            animation.Rebind();
            animation.Play("New Animation");
        }
    }

    protected override void OnAnimationFinished()
    {
        base.OnAnimationFinished();

        m_Translate = false;
    }

    private void PutArrowsOnInitialPosition()
    {
        var arrow = transform.GetChild(0);
        arrow.localPosition = new Vector3(arrow.localPosition.x, 2.5f, arrow.localPosition.z);

        arrow = transform.GetChild(1);
        arrow.localPosition = new Vector3(arrow.localPosition.x, 1f, arrow.localPosition.z);

        arrow = transform.GetChild(2);
        arrow.localPosition = new Vector3(arrow.localPosition.x, -0.5f, arrow.localPosition.z);

        arrow = transform.GetChild(3);
        arrow.localPosition = new Vector3(arrow.localPosition.x, -2f, arrow.localPosition.z);
    }
}
