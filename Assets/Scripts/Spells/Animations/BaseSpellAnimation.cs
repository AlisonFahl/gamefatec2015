﻿using UnityEngine;
using System.Collections;

public abstract class BaseSpellAnimation : MonoBehaviour {
    [SerializeField]
    protected float m_AnimationTime;

    void Start()
    {
        //m_Animator.Stop();
        gameObject.SetActive(false);
    }

    public void Play()
    {
        gameObject.SetActive(true);

        OnAnimationStart();

        StartCoroutine(OnFinished());
    }

    protected virtual void OnAnimationStart()
    {
        gameObject.SetActive(true);
    }

    protected virtual void OnAnimationFinished()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator OnFinished()
    {
        yield return new WaitForSeconds(m_AnimationTime);

        OnAnimationFinished();
    }
}
