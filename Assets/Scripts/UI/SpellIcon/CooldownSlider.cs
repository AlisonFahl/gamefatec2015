﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CooldownSlider : MonoBehaviour {

    private Slider m_Slider = null;
    private float m_Time = 0f, m_CurrentTime = 0f;
    private bool m_IsInCooldown = false;

	// Use this for initialization
	void Start () 
    {
        m_Slider = GetComponent<Slider>();
        m_Slider.value = 0;
	}


	// Update is called once per frame
	void Update () 
    {
        if (m_IsInCooldown)
        {
            m_CurrentTime -= Time.deltaTime;
            if (m_CurrentTime <= 0)
            {
                m_CurrentTime = 0;
                m_IsInCooldown = false;
            }

            m_Slider.value = m_CurrentTime / m_Time;
        }

	}

    public void StartCooldown(float time)
    {
        if(time <= 0)
        {
            throw new Exception("time connot be negative or zero");
        }

        m_Time = time;
        m_CurrentTime = m_Time;
        m_Slider.value = 1;
        m_IsInCooldown = true;
    }

    public bool IsInCooldown()
    {
        return m_IsInCooldown;
    }
}
