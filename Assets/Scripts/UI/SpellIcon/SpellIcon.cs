﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpellIcon : MonoBehaviour {

    private float m_CooldownTime = 5.0f;
    private CooldownSlider m_CooldownSlider = null;

    [SerializeField]
    private Sprite m_EnabledOutline;
    [SerializeField]
    private Sprite m_DisabledOutline;

    void Awake()
    {
        m_CooldownSlider = GetComponentInChildren<CooldownSlider>();

        transform.Find("Outline").GetComponent<Image>().sprite = m_DisabledOutline;
    }

    public void ActivateSpell()
    {
        if (m_EnabledOutline)
        {
            transform.Find("Outline").GetComponent<Image>().sprite = m_EnabledOutline;
        }
        //TODO: play animation
    }

    public void DeactivateSpell()
    {
        if (m_DisabledOutline)
        {
            transform.Find("Outline").GetComponent<Image>().sprite = m_DisabledOutline;
        }
        //TODO: play animation
    }

    /*
     * Returns true if was able to cast and false otherwise. The spell is able to be cast whenever it is not in cooldown
     */
    public bool CastSpell()
    {
        if (CanBeCast())
        {
            m_CooldownSlider.StartCooldown(m_CooldownTime);

            return true;
        }

        return false;
    }

    /*
     * Returns true if the spell is not in cooldown and false otherwise
     */
    public bool CanBeCast()
    {
        return !m_CooldownSlider.IsInCooldown();
    }
}
