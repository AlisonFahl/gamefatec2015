﻿using UnityEngine;
using System.Collections;

public class Action : MonoBehaviour {

    private static readonly Vector2 VECTOR_CAST = new Vector2(0, 1);
    private static readonly Vector2 VECTOR_CANCEL = new Vector2(0, -1);
    private static readonly float MAX_ANGLE = 40f;
    private static readonly float LENGTH = 90f;

    private bool m_CalculatingInput = false;
    private Vector2 m_InputVector;
    private Vector2 m_OldMousePosition;

    private PlayerController m_PlayerController;

    private float m_IdleTime = 0;
    private bool m_FirstSpell = true, m_PlayingAnimation = false;

    void Start()
    {
        

        var player = GameObject.Find("Player");
        m_PlayerController = player.GetComponent<PlayerController>();
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            m_IdleTime = 0;

            m_OldMousePosition = Input.mousePosition;
            m_InputVector = Vector2.zero;
            m_CalculatingInput = true;
        }
        else if(m_CalculatingInput)
        {
            Vector2 deltaMouseMovement = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - m_OldMousePosition;

            m_InputVector += deltaMouseMovement;
            if(m_InputVector.magnitude > LENGTH)
            {
                StopAnimation();
                m_PlayingAnimation = false;

                //Cast Spell
                if (Mathf.Abs(Vector2.Angle(m_InputVector, VECTOR_CAST)) <= MAX_ANGLE)
                {
                    m_PlayerController.CastSpell();
                }
                //CancelSpell
                else if (Mathf.Abs(Vector2.Angle(m_InputVector, VECTOR_CANCEL)) <= MAX_ANGLE)
                {
                    m_PlayerController.CancelSpell();
                }

                m_CalculatingInput = false;
            }
        }

        if(Input.GetAxis("Cast") > 0)
        {
            m_PlayerController.CastSpell();
        }

        if (Input.GetAxis("Cancel") > 0)
        {
            m_PlayerController.CancelSpell();
        }

        if(Input.GetMouseButtonUp(0))
        {
            m_CalculatingInput = false;
        }

        if(m_PlayerController.HasSelectedSpell())
        {
            m_IdleTime += Time.deltaTime;
            //play arrow animation
            if ((m_IdleTime >= 5 || m_FirstSpell) && !m_PlayingAnimation)
            {
                m_FirstSpell = false;

                //PlayAnimation();

                m_PlayingAnimation = true;
            }
        }
    }

    private void PlayAnimation()
    {
        transform.Find("CastArrow").gameObject.GetComponent<ArrowAnimation>().StartAnimation();
        transform.Find("CancelArrow").gameObject.GetComponent<ArrowAnimation>().StartAnimation();
    }

    private void StopAnimation()
    {
        transform.Find("CastArrow").gameObject.GetComponent<ArrowAnimation>().StopAnimation();
        transform.Find("CancelArrow").gameObject.GetComponent<ArrowAnimation>().StopAnimation();
    }
}
