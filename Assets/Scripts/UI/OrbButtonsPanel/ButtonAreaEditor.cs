﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(OrbButton))]
public class ButtonAreaEditor : Editor {

	void OnSceneGUI()
    {
        OrbButton button = (OrbButton)target;

        Handles.DrawWireDisc(button.transform.position, button.transform.forward, button.m_Radious);
    }
}
#endif