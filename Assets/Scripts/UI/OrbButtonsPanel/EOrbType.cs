﻿using UnityEngine;

public enum EOrbType{
    A = 0, 
    
    B = 3, 
    
    C = 27
}

public static class EOrbTypeExtentions
{
    public static KeyCode GetKeyCode(this EOrbType thisOrb)
    {
        switch (thisOrb)
        {
            case EOrbType.A:
                return KeyCode.Joystick1Button2;
            case EOrbType.B:
                return KeyCode.Joystick1Button0;
            case EOrbType.C:
                return KeyCode.Joystick1Button1;
            default:
                return KeyCode.Joystick1Button1;
        }
    }
}