﻿using UnityEngine;
using System.Collections;

public class SelectionBar : MonoBehaviour {

    private GameObject[] m_SelectionFillAreas;

    void Awake()
    {
        m_SelectionFillAreas = new GameObject[3];

        for(int i=0; i<m_SelectionFillAreas.Length; i++)
        {
            m_SelectionFillAreas[i] = transform.GetChild(i).gameObject;
        }
    }

    void Start()
    {
        DisableAll();
    }

	public void OnOrbSelected()
    {
        EnableNext();
    }

    public void ClearSelection()
    {
        DisableAll();
    }

    private void DisableAll()
    {
        foreach(var gameObject in m_SelectionFillAreas)
        {
            gameObject.SetActive(false);
            //TODO: play deactivation animation
        }
    }

    private void EnableNext()
    {
        for (int i = 0; i < m_SelectionFillAreas.Length; i++)
        {
            if(!m_SelectionFillAreas[i].activeInHierarchy)
            {
                m_SelectionFillAreas[i].SetActive(true);
                //TODO: play activation animation
                break;
            }
        }
    }
}
