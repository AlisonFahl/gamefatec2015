﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour 
{
    [SerializeField]
    private float m_Health = 250;

    [SerializeField]
    private float m_Score = 0;

    public void TakeDamage(float quantity)
    {
        m_Health -= quantity;

        UpdateLifeText();

        CheckDeath();
    }

    public void GainPoints(float quantity)
    {
        m_Score += quantity;

        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        GameObject.Find("Score").GetComponent<Text>().text = "Score " + m_Score;
    }

    private void UpdateLifeText()
    {
        GameObject.Find("Life").GetComponent<Text>().text = @"250/" + m_Health;
    }

    public void CheckDeath()
    {
        if(m_Health <= 0)
        {
            OnDeath();
        }
    }

    private void OnDeath()
    {

    }
}
