﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

    private OrbSpellRelationDictionary m_OrbSpellRelationDictionary;

    private Spell m_SelectedSpell = null;

    void Awake()
    {
        m_OrbSpellRelationDictionary = GetComponent<OrbSpellRelationDictionary>();
    }

    public void SetSelectedSpell(int combinationId)
    {
        m_SelectedSpell = m_OrbSpellRelationDictionary.GetSpell(combinationId);
        m_SelectedSpell.SetAsCurrentSpell();
    }

    public void CancelSpell()
    {
        if (HasSelectedSpell())
        {
            UnselectSpell();
        }

        ClearOrbSelection();
    }

    /*
     * Returns wether the Player Controller was able to cast the spell or not.
     */
    public bool CastSpell()
    {
        if(HasSelectedSpell())
        {
            var val = m_SelectedSpell.CastSpell();
            UnselectSpell();
            ClearOrbSelection();

            return val;
        }

        return false;
    }

    public bool HasSelectedSpell()
    {
        return m_SelectedSpell;
    }

    private void UnselectSpell()
    {
        m_SelectedSpell.UnselectSpell();
        m_SelectedSpell = null;
    }

    private void ClearOrbSelection()
    {
        //Reset any selected orb in the buttons panel
        GameObject.Find("ButtonsPanel").GetComponent<OrbButtonsPanel>().Refresh();
    }
}
